mod ast;
mod lexer;
mod parser;
pub mod repl;
mod token;

#[warn(clippy::all, clippy::pedantic, clippy::nursery, clippy::cargo)]
#[cfg(test)]
mod tests {
    use crate::{
        ast::{
            BlockStatement, BooleanExpression, CallExpression, Expression, ExpressionStatement,
            FnLiteralExpression, IdentifierExpression, IfExpression, InfixExpression,
            IntegerExpression, LetStatement, PrefixExpression, ReturnStatement, Statement,
        },
        lexer::Lexer,
        parser::Parser,
        token::{Token, TokenType},
    };

    #[test]
    fn test_lexer() {
        let input = "let five = 5;
        let ten = 10;

        let add = fn(x, y) {
            x + y;
        };

        let result = add(five, ten);
        !*-/5;
        5 < 10 > 5;

        if (5 < 10) {
            return true;
        } else {
            return false;
        }

        10 == 10;
        10 != 9;";

        let lexer = Lexer::new(input);
        let expected = vec![
            Token::from_str(TokenType::Let, "let"),
            Token::from_str(TokenType::Identifier, "five"),
            Token::from_char(TokenType::Assign, '='),
            Token::from_str(TokenType::Integer, "5"),
            Token::from_char(TokenType::Semicolon, ';'),
            Token::from_str(TokenType::Let, "let"),
            Token::from_str(TokenType::Identifier, "ten"),
            Token::from_char(TokenType::Assign, '='),
            Token::from_str(TokenType::Integer, "10"),
            Token::from_char(TokenType::Semicolon, ';'),
            Token::from_str(TokenType::Let, "let"),
            Token::from_str(TokenType::Identifier, "add"),
            Token::from_char(TokenType::Assign, '='),
            Token::from_str(TokenType::Function, "fn"),
            Token::from_char(TokenType::LParen, '('),
            Token::from_str(TokenType::Identifier, "x"),
            Token::from_char(TokenType::Comma, ','),
            Token::from_str(TokenType::Identifier, "y"),
            Token::from_char(TokenType::RParen, ')'),
            Token::from_char(TokenType::LBrace, '{'),
            Token::from_str(TokenType::Identifier, "x"),
            Token::from_char(TokenType::Plus, '+'),
            Token::from_str(TokenType::Identifier, "y"),
            Token::from_char(TokenType::Semicolon, ';'),
            Token::from_char(TokenType::RBrace, '}'),
            Token::from_char(TokenType::Semicolon, ';'),
            Token::from_str(TokenType::Let, "let"),
            Token::from_str(TokenType::Identifier, "result"),
            Token::from_char(TokenType::Assign, '='),
            Token::from_str(TokenType::Identifier, "add"),
            Token::from_char(TokenType::LParen, '('),
            Token::from_str(TokenType::Identifier, "five"),
            Token::from_char(TokenType::Comma, ','),
            Token::from_str(TokenType::Identifier, "ten"),
            Token::from_char(TokenType::RParen, ')'),
            Token::from_char(TokenType::Semicolon, ';'),
            Token::from_char(TokenType::Bang, '!'),
            Token::from_char(TokenType::Asterisk, '*'),
            Token::from_char(TokenType::Minus, '-'),
            Token::from_char(TokenType::Slash, '/'),
            Token::from_str(TokenType::Integer, "5"),
            Token::from_char(TokenType::Semicolon, ';'),
            Token::from_str(TokenType::Integer, "5"),
            Token::from_char(TokenType::LessThan, '<'),
            Token::from_str(TokenType::Integer, "10"),
            Token::from_char(TokenType::GreaterThan, '>'),
            Token::from_str(TokenType::Integer, "5"),
            Token::from_char(TokenType::Semicolon, ';'),
            Token::from_str(TokenType::If, "if"),
            Token::from_char(TokenType::LParen, '('),
            Token::from_str(TokenType::Integer, "5"),
            Token::from_char(TokenType::LessThan, '<'),
            Token::from_str(TokenType::Integer, "10"),
            Token::from_char(TokenType::RParen, ')'),
            Token::from_char(TokenType::LBrace, '{'),
            Token::from_str(TokenType::Return, "return"),
            Token::from_str(TokenType::True, "true"),
            Token::from_char(TokenType::Semicolon, ';'),
            Token::from_char(TokenType::RBrace, '}'),
            Token::from_str(TokenType::Else, "else"),
            Token::from_char(TokenType::LBrace, '{'),
            Token::from_str(TokenType::Return, "return"),
            Token::from_str(TokenType::False, "false"),
            Token::from_char(TokenType::Semicolon, ';'),
            Token::from_char(TokenType::RBrace, '}'),
            Token::from_str(TokenType::Integer, "10"),
            Token::from_str(TokenType::Equal, "=="),
            Token::from_str(TokenType::Integer, "10"),
            Token::from_char(TokenType::Semicolon, ';'),
            Token::from_str(TokenType::Integer, "10"),
            Token::from_str(TokenType::NotEqual, "!="),
            Token::from_str(TokenType::Integer, "9"),
            Token::from_char(TokenType::Semicolon, ';'),
            Token::from_char(TokenType::Eof, '\0'),
        ];
        assert_eq!(lexer.tokenize(), expected);
    }

    #[test]
    fn test_parse_let_statements() {
        let input = "let x = 5;
let y = true;
let foobar = y;";

        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer);

        let expected = vec![
            Statement::Let(LetStatement {
                name: IdentifierExpression {
                    value: String::from("x"),
                },
                value: Expression::Integer(IntegerExpression { value: 5 }),
            }),
            Statement::Let(LetStatement {
                name: IdentifierExpression {
                    value: String::from("y"),
                },
                value: Expression::Boolean(BooleanExpression { value: true }),
            }),
            Statement::Let(LetStatement {
                name: IdentifierExpression {
                    value: String::from("foobar"),
                },
                value: Expression::Identifier(IdentifierExpression {
                    value: "y".to_string(),
                }),
            }),
        ];
        let parsed_input = parser.parse_program().unwrap();
        assert_eq!(*parsed_input, expected);
        assert_eq!(parsed_input.to_string(), input.to_string());
    }

    #[test]
    fn test_parse_return_statements() {
        let input = "return 5;
return true;
return x;";

        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer);

        let expected = vec![
            Statement::Return(ReturnStatement {
                value: Expression::Integer(IntegerExpression { value: 5 }),
            }),
            Statement::Return(ReturnStatement {
                value: Expression::Boolean(BooleanExpression { value: true }),
            }),
            Statement::Return(ReturnStatement {
                value: Expression::Identifier(IdentifierExpression {
                    value: "x".to_string(),
                }),
            }),
        ];
        let parsed_input = parser.parse_program().unwrap();
        assert_eq!(*parsed_input, expected);
        assert_eq!(parsed_input.to_string(), input.to_string());
    }

    #[test]
    fn test_parse_expression_statements() {
        let input = "foobar;";

        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer);

        let expected = vec![Statement::Expression(ExpressionStatement {
            expr: Expression::Identifier(IdentifierExpression {
                value: String::from("foobar"),
            }),
        })];
        let parsed_input = parser.parse_program().unwrap();
        assert_eq!(*parsed_input, expected);
        //assert_eq!(parsed_input.to_string(), input.to_string());
    }

    #[test]
    fn test_parse_integer_expression() {
        let input = "5;";

        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer);

        let expected = vec![Statement::Expression(ExpressionStatement {
            expr: Expression::Integer(IntegerExpression { value: 5 }),
        })];
        let parsed_input = parser.parse_program().unwrap();
        assert_eq!(*parsed_input, expected);
        //assert_eq!(parsed_input.to_string(), input.to_string());
    }

    #[test]
    fn test_parse_prefix_bang_expression() {
        let input = "!5;";

        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer);

        let expected = vec![Statement::Expression(ExpressionStatement {
            expr: Expression::Prefix(PrefixExpression {
                prefix: Token {
                    kind: TokenType::Bang,
                    literal: '!'.to_string(),
                },
                expr: Box::new(Expression::Integer(IntegerExpression { value: 5 })),
            }),
        })];
        let parsed_input = parser.parse_program().unwrap();
        assert_eq!(*parsed_input, expected);
        //assert_eq!(parsed_input.to_string(), input.to_string());
    }

    #[test]
    fn test_parse_prefix_minus_expression() {
        let input = "-12;";

        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer);

        let expected = vec![Statement::Expression(ExpressionStatement {
            expr: Expression::Prefix(PrefixExpression {
                prefix: Token {
                    kind: TokenType::Minus,
                    literal: '-'.to_string(),
                },
                expr: Box::new(Expression::Integer(IntegerExpression { value: 12 })),
            }),
        })];
        let parsed_input = parser.parse_program().unwrap();
        assert_eq!(*parsed_input, expected);
        //assert_eq!(parsed_input.to_string(), input.to_string());
    }

    #[test]
    fn test_parse_infix_expressions() {
        let inputs = ["5 + 6;", "5 - 6;", "5 == 6;", "5 != 6;"];
        let expected_operators = [
            Token {
                kind: TokenType::Plus,
                literal: '+'.to_string(),
            },
            Token {
                kind: TokenType::Minus,
                literal: '-'.to_string(),
            },
            Token {
                kind: TokenType::Equal,
                literal: "==".to_string(),
            },
            Token {
                kind: TokenType::NotEqual,
                literal: "!=".to_string(),
            },
        ];

        for (i, input) in inputs.iter().enumerate() {
            let lexer = Lexer::new(input);
            let mut parser = Parser::new(lexer);

            let expected = vec![Statement::Expression(ExpressionStatement {
                expr: Expression::Infix(InfixExpression {
                    operator: expected_operators[i].clone(),
                    lhs: Box::new(Expression::Integer(IntegerExpression { value: 5 })),
                    rhs: Box::new(Expression::Integer(IntegerExpression { value: 6 })),
                }),
            })];
            let parsed_input = parser.parse_program().unwrap();
            assert_eq!(*parsed_input, expected);
            //assert_eq!(parsed_input.to_string(), input.to_string());
        }
    }

    #[test]
    fn test_operator_precedence_parsing() {
        let inputs = [
            "-a * b;",
            "!-a;",
            "a + b + c;",
            "true;",
            "false;",
            "3 > 5 == false;",
            "1 + (2 + 3) + 4;",
            "(5 + 5) * 2;",
            "2 / (5 + 5);",
            "-(5 + 5);",
            "!(true == true);",
            "a + add(b * c) + d;",
            "add(a, b, 1, 2 * 3, 4 + 5, add(6, 7 * 8));",
            "add(a + b + c * d / f + g);",
        ];
        let expected_strings = [
            "((-a) * b);",
            "(!(-a));",
            "((a + b) + c);",
            "true;",
            "false;",
            "((3 > 5) == false);",
            "((1 + (2 + 3)) + 4);",
            "((5 + 5) * 2);",
            "(2 / (5 + 5));",
            "(-(5 + 5));",
            "(!(true == true));",
            "((a + add((b * c))) + d);",
            "add(a, b, 1, (2 * 3), (4 + 5), add(6, (7 * 8)));",
            "add((((a + b) + ((c * d) / f)) + g));",
        ];

        for (i, input) in inputs.iter().enumerate() {
            let lexer = Lexer::new(input);
            let mut parser = Parser::new(lexer);

            let parsed_input = parser.parse_program().unwrap();
            assert_eq!(parsed_input.to_string(), expected_strings[i].to_string());
        }
    }

    #[test]
    fn test_parse_boolean_expression() {
        let inputs = ["true;", "false;"];
        let expected_values = [
            BooleanExpression { value: true },
            BooleanExpression { value: false },
        ];

        for (i, input) in inputs.iter().enumerate() {
            let lexer = Lexer::new(input);
            let mut parser = Parser::new(lexer);

            let expected = vec![Statement::Expression(ExpressionStatement {
                expr: Expression::Boolean(expected_values[i].clone()),
            })];
            let parsed_input = parser.parse_program().unwrap();
            assert_eq!(*parsed_input, expected);
            //assert_eq!(parsed_input.to_string(), input.to_string());
        }
    }

    #[test]
    fn test_parse_if_expression_no_alternative() {
        let input = "if (x > y) { x };";

        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer);

        let expected = vec![Statement::Expression(ExpressionStatement {
            expr: Expression::If(IfExpression {
                condition: Box::new(Expression::Infix(InfixExpression {
                    operator: Token {
                        kind: TokenType::GreaterThan,
                        literal: '>'.to_string(),
                    },
                    lhs: Box::new(Expression::Identifier(IdentifierExpression {
                        value: "x".to_string(),
                    })),
                    rhs: Box::new(Expression::Identifier(IdentifierExpression {
                        value: "y".to_string(),
                    })),
                })),
                consequence: BlockStatement {
                    statements: vec![Statement::Expression(ExpressionStatement {
                        expr: Expression::Identifier(IdentifierExpression {
                            value: "x".to_string(),
                        }),
                    })],
                },
                alternative: None,
            }),
        })];
        let parsed_input = parser.parse_program().unwrap();
        assert_eq!(*parsed_input, expected);
        //assert_eq!(parsed_input.to_string(), input.to_string());
    }

    #[test]
    fn test_parse_if_expression_with_alternative() {
        let input = "if (x > y) { x } else { y };";

        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer);

        let expected = vec![Statement::Expression(ExpressionStatement {
            expr: Expression::If(IfExpression {
                condition: Box::new(Expression::Infix(InfixExpression {
                    operator: Token {
                        kind: TokenType::GreaterThan,
                        literal: '>'.to_string(),
                    },
                    lhs: Box::new(Expression::Identifier(IdentifierExpression {
                        value: "x".to_string(),
                    })),
                    rhs: Box::new(Expression::Identifier(IdentifierExpression {
                        value: "y".to_string(),
                    })),
                })),
                consequence: BlockStatement {
                    statements: vec![Statement::Expression(ExpressionStatement {
                        expr: Expression::Identifier(IdentifierExpression {
                            value: "x".to_string(),
                        }),
                    })],
                },
                alternative: Some(BlockStatement {
                    statements: vec![Statement::Expression(ExpressionStatement {
                        expr: Expression::Identifier(IdentifierExpression {
                            value: "y".to_string(),
                        }),
                    })],
                }),
            }),
        })];
        let parsed_input = parser.parse_program().unwrap();
        assert_eq!(*parsed_input, expected);
        //assert_eq!(parsed_input.to_string(), input.to_string());
    }

    #[test]
    fn test_parse_function_literal() {
        let input = "fn(x, y) { x + y; }";

        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer);

        let expected = vec![Statement::Expression(ExpressionStatement {
            expr: Expression::FnLiteral(FnLiteralExpression {
                parameters: vec![
                    IdentifierExpression {
                        value: "x".to_string(),
                    },
                    IdentifierExpression {
                        value: "y".to_string(),
                    },
                ],
                body: BlockStatement {
                    statements: vec![Statement::Expression(ExpressionStatement {
                        expr: Expression::Infix(InfixExpression {
                            operator: Token {
                                kind: TokenType::Plus,
                                literal: '+'.to_string(),
                            },
                            lhs: Box::new(Expression::Identifier(IdentifierExpression {
                                value: "x".to_string(),
                            })),
                            rhs: Box::new(Expression::Identifier(IdentifierExpression {
                                value: "y".to_string(),
                            })),
                        }),
                    })],
                },
            }),
        })];
        let parsed_input = parser.parse_program().unwrap();
        assert_eq!(*parsed_input, expected);
        //assert_eq!(parsed_input.to_string(), input.to_string());
    }

    #[test]
    fn test_parse_function_parameters() {
        let inputs = ["fn() {}", "fn(x) {};", "fn (x, y) {}"];
        let expected_parameters = [
            vec![],
            vec![IdentifierExpression {
                value: "x".to_string(),
            }],
            vec![
                IdentifierExpression {
                    value: "x".to_string(),
                },
                IdentifierExpression {
                    value: "y".to_string(),
                },
            ],
        ];

        for (i, input) in inputs.iter().enumerate() {
            let lexer = Lexer::new(input);
            let mut parser = Parser::new(lexer);

            let expected = vec![Statement::Expression(ExpressionStatement {
                expr: Expression::FnLiteral(FnLiteralExpression {
                    parameters: expected_parameters[i].clone(),
                    body: BlockStatement { statements: vec![] },
                }),
            })];
            let parsed_input = parser.parse_program().unwrap();
            assert_eq!(*parsed_input, expected);
            //assert_eq!(parsed_input.to_string(), input.to_string());
        }
    }

    #[test]
    fn test_parse_call_expression() {
        let input = "add(1, 2 * 3, 4 + 5)";

        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer);

        let expected = vec![Statement::Expression(ExpressionStatement {
            expr: Expression::Call(CallExpression {
                function: Box::new(Expression::Identifier(IdentifierExpression {
                    value: "add".to_string(),
                })),
                arguments: vec![
                    Expression::Integer(IntegerExpression { value: 1 }),
                    Expression::Infix(InfixExpression {
                        operator: Token {
                            kind: TokenType::Asterisk,
                            literal: '*'.to_string(),
                        },
                        lhs: Box::new(Expression::Integer(IntegerExpression { value: 2 })),
                        rhs: Box::new(Expression::Integer(IntegerExpression { value: 3 })),
                    }),
                    Expression::Infix(InfixExpression {
                        operator: Token {
                            kind: TokenType::Plus,
                            literal: '+'.to_string(),
                        },
                        lhs: Box::new(Expression::Integer(IntegerExpression { value: 4 })),
                        rhs: Box::new(Expression::Integer(IntegerExpression { value: 5 })),
                    }),
                ],
            }),
        })];
        let parsed_input = parser.parse_program().unwrap();
        assert_eq!(*parsed_input, expected);
        //assert_eq!(parsed_input.to_string(), input.to_string());
    }
}
